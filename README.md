## Table of Contents
***
1. [General Information](#general-information)
2. [Technologies](#technologies)
3. [Installation](#installation)
3. [Configuration](#configuration)
4. [Documentation](#documentation)
## General Information
***
This REST API was created as a challenge proposed by [MinData](https://www.mindata.es/) company. 

## Technologies
***
A list of technologies used within the project:
* [SpringBoot](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/)
* [JPA](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#reference)
* [H2 Data Base](https://www.h2database.com/html/main.html)
* [Spring Cache](https://spring.io/guides/gs/caching/)
* [Flyway](https://flywaydb.org/documentation/)
* [Docker](https://docs.docker.com/)
* [Logback](https://logback.qos.ch/documentation.html) 
* [Mockito](https://javadoc.io/doc/org.mockito/mockito-core/latest/org/mockito/Mockito.html) 
* [JUnit](https://junit.org/junit5/docs/current/user-guide/) 
## Installation
***
A little intro about the installation. 
```
$ git clone https://gitlab.com/nachoLopezArg/mindatachallange.git
$ cd ../path/to/the/file
$ mvn clean install
```
## Configuration
Properties files are used to keep ‘N’ number of properties in a single file to run the application in a different environment. In Spring Boot, properties are kept in the application.properties file under the classpath.

The application.properties file is located in the src/main/resources directory. The code for sample application.properties file is given below −
```
server.port=${es.mindata.challenge.port:8085}

# Enabling H2 Console
spring.h2.console.enabled=true

# H2 DB
spring.datasource.driver-class-name=${es.mindata.challenge.datasource.class:org.h2.Driver}
spring.datasource.username=${es.mindata.challenge.datasource.username:sa}
spring.datasource.password=${es.mindata.challenge.datasource.password:password}
spring.datasource.url=${es.mindata.challenge.datasource.url:jdbc:h2:mem:mindata}
spring.jpa.hibernate.ddl-auto=${es.mindata.challenge.hibernate.ddl:validate}
spring.jpa.database-platform=${es.mindata.challenge.hibernate.dialect:org.hibernate.dialect.H2Dialect}

#LOGS LEVELS
logging.level.org.springframework.messaging=${es.mindata.challenge.logs.messaging:info}
logging.level.org.springframework.web.socket=${es.mindata.challenge.logs.websocket:info}
logging.level.org.springframework.web.socket.sockjs.transport.session=${es.mindata.challenge.logs.rabbit:info}

```

## Documentation
Documentation are auto-generated from an API definition. Swagger Core generates the OpenAPI definitions from existing (Java) APIs code. 
* [Swagger Url](https://localhost:8085/swagger-ui.html/)

Side information: This proyect has a dummy in memmory user and password to authenticate REST API & Swagger.
```
username: admin
password: admin
```
