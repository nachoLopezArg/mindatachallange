package es.mindata.challenge.aspect;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.TemporalUnit;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.annotation.Configuration;

@Aspect
@Configuration
public class MethodExecutionTimeAspect {
    private Logger        logger = LoggerFactory.getLogger(this.getClass());
    private LocalDateTime spentTime;

    @After("execution(* es.mindata.challenge.controller.*.*(..))")
    public void after(JoinPoint joinPoint) {

        logger.info(String.format("Spent %.0fms executing API method: %s",
                                  Duration.between(this.spentTime, LocalDateTime.now()).getNano()*0.000001,
                                  joinPoint.getSignature()));
        this.spentTime = LocalDateTime.now();
    }

    @Before("execution(* es.mindata.challenge.controller.*.*(..))")
    public void before(JoinPoint joinPoint) {
        logger.info(String.format("Executing API method: %s", joinPoint.getSignature()));
        this.spentTime = LocalDateTime.now();
    }
}

