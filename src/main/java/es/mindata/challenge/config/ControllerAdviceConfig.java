package es.mindata.challenge.config;

import es.mindata.challenge.model.exception.DuplicatedException;
import es.mindata.challenge.model.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;
import javax.xml.bind.ValidationException;

@ControllerAdvice
public class ControllerAdviceConfig {

    public final static Logger LOGGER = LoggerFactory.getLogger(ControllerAdviceConfig.class);

    @ExceptionHandler(value = ConstraintViolationException.class)
    public ResponseEntity validationException(ConstraintViolationException e) {
        LOGGER.debug(e.getLocalizedMessage());
        return ResponseEntity.badRequest().build();
    }

    @ExceptionHandler(value = DuplicatedException.class)
    public ResponseEntity duplicatedException(DuplicatedException e) {
        LOGGER.debug(e.getLocalizedMessage());
        return ResponseEntity.status(HttpStatus.CONFLICT).build();
    }

    @ExceptionHandler(value = ValidationException.class)
    public ResponseEntity validationException(ValidationException e) {
        LOGGER.debug(e.getLocalizedMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    @ExceptionHandler(value = NotFoundException.class)
    public ResponseEntity notFoundException(NotFoundException e) {
        LOGGER.debug(e.getLocalizedMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

}


