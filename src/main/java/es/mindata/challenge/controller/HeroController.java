package es.mindata.challenge.controller;

import es.mindata.challenge.model.dto.HeroRequestDto;
import es.mindata.challenge.model.entity.Hero;
import es.mindata.challenge.service.HeroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.util.List;

@RestController()
@RequestMapping(
    value   = "/api/v1/hero",
    headers = "Accept=application/json"
)
@Validated
public class HeroController {
    @Autowired
    private HeroService heroService;

    @PostMapping()
    public Hero createHero(@Valid
    @RequestBody HeroRequestDto hero) throws Exception {
        return this.heroService.create(hero);
    }

    @DeleteMapping("/{id}")
    @CacheEvict(allEntries = true, value = {"hero"})
    public void deleteHero(@PathVariable("id") String id) throws Exception {
        this.heroService.delete(Long.valueOf(id));
    }

    @PutMapping("/{id}")
    @CacheEvict(allEntries = true, value = {"hero"})
    public void updateHero(@PathVariable("id") String id, @Valid
    @RequestBody HeroRequestDto hero) throws Exception {
        this.heroService.update(Long.valueOf(id), hero);
    }

    @GetMapping(value = "/all",params = {"page","sort"})
    public Page<Hero> getAll(@RequestParam(defaultValue = "0") int page, @Pattern(regexp = "(asc|desc)")
    @RequestParam(defaultValue = "desc") String sort) {
        return this.heroService.getAll(PageRequest.of(page, 30, Sort.by(sort.equals("desc")
                                                                        ? Sort.Direction.DESC
                                                                        : Sort.Direction.ASC, "name")));
    }

    @GetMapping("/name")
    public List<String> getAllNames() {
        return this.heroService.getAllNames();
    }

    @GetMapping("/{id}")
    @Cacheable(cacheNames = "hero", key="#id")
    public Hero getById(@PathVariable("id") Long id) throws Exception {
        return this.heroService.getByID(id);
    }

    @GetMapping(params = { "name" })
    @Cacheable(cacheNames = "herosContainingName", key="#name")
    public List<Hero> getByName(@RequestParam String name) {
        return this.heroService.getByName(name);
    }
}


