package es.mindata.challenge.model.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HeroRequestDto {
    @Pattern(regexp = "^[A-Za-z]*$")
    @NotNull
    @NotBlank
    private String name;
}


