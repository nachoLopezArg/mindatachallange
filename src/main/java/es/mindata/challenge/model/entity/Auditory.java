package es.mindata.challenge.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.LocalDateTime;

@Getter
@Setter
@MappedSuperclass
public abstract class Auditory {
    LocalDateTime fechaCreacion;
    LocalDateTime fechaModificacion;

    @PrePersist
    void actualizarFechaCreacion() {
        this.fechaCreacion = LocalDateTime.now();
    }

    @PreUpdate
    void actualizarFechaModificacion() {
        this.fechaModificacion = LocalDateTime.now();
    }
}


