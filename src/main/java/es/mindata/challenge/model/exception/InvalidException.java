package es.mindata.challenge.model.exception;

import javax.xml.bind.ValidationException;

public class InvalidException extends ValidationException {
    public InvalidException(String message) {
        super(message);
    }

    public InvalidException(Throwable exception) {
        super(exception);
    }

    public InvalidException(String message, String errorCode) {
        super(message, errorCode);
    }

    public InvalidException(String message, Throwable exception) {
        super(message, exception);
    }

    public InvalidException(String message, String errorCode, Throwable exception) {
        super(message, errorCode, exception);
    }
}


