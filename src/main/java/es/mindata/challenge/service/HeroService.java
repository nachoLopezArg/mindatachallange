package es.mindata.challenge.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import es.mindata.challenge.model.dto.HeroRequestDto;
import es.mindata.challenge.model.entity.Hero;
import es.mindata.challenge.model.exception.DuplicatedException;
import es.mindata.challenge.model.exception.InvalidException;
import es.mindata.challenge.model.exception.NotFoundException;

public interface HeroService {
    Hero create(HeroRequestDto hero) throws DuplicatedException, InvalidException;

    void delete(Long id) throws NotFoundException;

    void update(Long id, HeroRequestDto hero) throws NotFoundException, InvalidException;

    Page<Hero> getAll(PageRequest filter);

    List<String> getAllNames();

    Hero getByID(Long id) throws NotFoundException, InvalidException;

    List<Hero> getByName(String name);

}


