package es.mindata.challenge.service;

import es.mindata.challenge.model.dto.HeroRequestDto;
import es.mindata.challenge.model.entity.Hero;
import es.mindata.challenge.model.exception.DuplicatedException;
import es.mindata.challenge.model.exception.InvalidException;
import es.mindata.challenge.model.exception.NotFoundException;
import es.mindata.challenge.repository.HeroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class HeroServiceImpl implements HeroService {
    @Autowired
    private HeroRepository heroRepository;

    @Override
    public Hero create(HeroRequestDto hero) {
        Hero h = new Hero();

        h.setName(hero.getName().strip());

        try {
            return this.heroRepository.save(h);
        } catch (DataIntegrityViolationException e) {
            throw new DuplicatedException(e);
        }
    }

    @Override
    public void delete(Long id) throws NotFoundException {
        try {
            this.heroRepository.deleteById(id);
        } catch (IllegalArgumentException e) {
            throw new NotFoundException(String.format("Provided id %d cant be null", id));
        } catch (EmptyResultDataAccessException e) {
            throw new NotFoundException(String.format("Provided id %d not found", id));
        }
    }

    @Override
    @Transactional
    public void update(Long id, HeroRequestDto hero) throws NotFoundException, InvalidException {
        try {
            this.heroRepository.findById(id)
                    .ifPresentOrElse(h -> h.setName(hero.getName().strip()),
                            () -> {
                                throw new NotFoundException();
                            });
        } catch (InvalidDataAccessApiUsageException e) {
            throw new InvalidException(e);
        }
    }

    @Override
    public Page<Hero> getAll(PageRequest filter) {
        return this.heroRepository.findAll(filter);
    }

    @Override
    public List<String> getAllNames() {
        return this.heroRepository.findAll().stream().map(Hero::getName).collect(Collectors.toList());
    }

    @Override
    public Hero getByID(Long id) throws InvalidException {
        try {
            return this.heroRepository.findById(id).orElseThrow(NotFoundException::new);
        } catch (InvalidDataAccessApiUsageException e) {
            throw new InvalidException(e);
        }
    }

    @Override
    public List<Hero> getByName(String name) {
        return this.heroRepository.findByNameContainingIgnoreCase(name);
    }
}


