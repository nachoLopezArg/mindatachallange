package es.mindata.challenge.controller;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.Mockito;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import es.mindata.challenge.config.ControllerAdviceConfig;
import es.mindata.challenge.model.entity.Hero;
import es.mindata.challenge.model.exception.InvalidException;
import es.mindata.challenge.model.exception.NotFoundException;
import es.mindata.challenge.service.HeroService;

@RunWith(SpringRunner.class)
public class HeroControllerTest {

    @TestConfiguration
    static class HeroControllerTestConfiguration {
        @Bean
        public MethodValidationPostProcessor bean() {
            return new MethodValidationPostProcessor();
        }

        @Bean
        public ControllerAdviceConfig controllerAdviceConfig() {
            return new ControllerAdviceConfig();
        }

        @Bean
        public HeroController heroController() {
            return new HeroController();
        }
    }

    @Autowired
    private HeroController         heroController;
    @Autowired
    private ControllerAdviceConfig controllerAdviceConfig;
    @MockBean
    private HeroService            heroService;
    private MockMvc                mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(heroController).setControllerAdvice(controllerAdviceConfig).build();
    }

    @Test
    public void createHero_withValidName_shouldReturn200() throws Exception {
        String url = "/api/v1/hero";

        Assert.assertNotNull(this.mockMvc.perform(post(url).contentType(MediaType.APPLICATION_JSON)
                                                              .content("{\"name\":\"wally\"}"))
                                         .andExpect(status().isOk()));
    }

    @Test
    public void createHero_withInvalidBody_shouldReturn400() throws Exception {
        Mockito.doThrow(NotFoundException.class).when(this.heroService).update(Mockito.any(), Mockito.any());

        String url = "/api/v1/hero";

        Assert.assertNotNull(this.mockMvc.perform(post(url, 5).contentType(MediaType.APPLICATION_JSON)
                                                              .content("{\"invalidProp\":\"wally\"}"))
                                         .andExpect(status().isBadRequest()));
    }

    @Test
    public void deleteHero_withValidId_shouldReturn200() throws Exception {
        String url = "/api/v1/hero/{id}";

        Assert.assertNotNull(this.mockMvc.perform(delete(url, 1)).andExpect(status().isOk()));
    }

    @Test
    public void deleteHero_withInvalidId_shouldReturn404() throws Exception {
        Mockito.doThrow(NotFoundException.class).when(this.heroService).delete(Mockito.any());

        String url = "/api/v1/hero/{id}";

        Assert.assertNotNull(this.mockMvc.perform(delete(url, 5)).andExpect(status().isNotFound()));
    }

    @Test
    public void updateHero_withValidName_shouldReturn200() throws Exception {
        String url = "/api/v1/hero/{id}";

        Assert.assertNotNull(this.mockMvc.perform(put(url, 4).contentType(MediaType.APPLICATION_JSON).content("{\"name\":\"wally\"}"))
                                         .andExpect(status().isOk()));
    }

    @Test
    public void updateHero_WithInvalidName_shouldReturn400() throws Exception {
        String url = "/api/v1/hero/{id}";

        Assert.assertNotNull(this.mockMvc.perform(put(url, 1).contentType(MediaType.APPLICATION_JSON)
                                                             .content("{\"name\":\"invalid-name-123\"}"))
                                         .andExpect(status().isBadRequest()));
    }

    @Test
    public void updateHero_WithInvalidId_shouldReurn404() throws Exception {
        Mockito.doThrow(NotFoundException.class).when(this.heroService).update(Mockito.any(), Mockito.any());

        String url = "/api/v1/hero/{id}";

        Assert.assertNotNull(this.mockMvc.perform(put(url, 3).contentType(MediaType.APPLICATION_JSON)
                                                             .content("{\"name\":\"wally\"}"))
                                         .andExpect(status().isNotFound()));
    }

    @Test
    public void getAll_shouldReturn200() throws Exception {

        Mockito.when(this.heroService.getAll(Mockito.any())).thenReturn(Page.empty());

        String url = "/api/v1/hero/all?page=0&sort=asc";

        Assert.assertNotNull(this.mockMvc.perform(get(url)).andExpect(status().isOk()));
    }

    @Test
    public void getAll_withStreamApi_shouldReturn200() throws Exception {

        Mockito.when(this.heroService.getAll(Mockito.any())).thenReturn(Page.empty());

        String url = "/api/v1/hero/name";

         this.mockMvc.perform(get(url)).andExpect(status().isOk()).andReturn();
    }

    @Test
    public void getAll_withInvalidSort_shouldReturn400() throws Exception {

        String url = "/api/v1/hero/all?page=0&sort=invalid";
        this.mockMvc.perform(get(url)).andExpect(status().isBadRequest()).andReturn();
    }

    @Test
    public void getById_withValidId_shouldReturn200() throws Exception {
        Mockito.when(this.heroService.getByID(Mockito.any())).thenReturn(new Hero());

        String url = "/api/v1/hero/{id}";

        Assert.assertNotNull(this.mockMvc.perform(get(url, 1)).andExpect(status().isOk()).andReturn());
    }

    @Test
    public void getById_withInvalidId_shouldReturn400() throws Exception {
        Mockito.when(this.heroService.getByID(Mockito.any())).thenThrow(new NotFoundException());

        String url = "/api/v1/hero/{id}";

        Assert.assertNotNull(this.mockMvc.perform(get(url, "asdfas")).andExpect(status().isBadRequest()));
    }

    @Test
    public void getById_withInvalidException_shouldReturn400() throws Exception {
        Mockito.when(this.heroService.getByID(Mockito.any())).thenThrow(new InvalidException(""));

        String url = "/api/v1/hero/{id}";

        Assert.assertNotNull(this.mockMvc.perform(get(url, "null")).andExpect(status().isBadRequest()));
    }

    @Test
    public void getById_withInvalidId_shouldReturn404() throws Exception {
        Mockito.when(this.heroService.getByID(Mockito.any())).thenThrow(new NotFoundException());

        String url = "/api/v1/hero/{id}";

        Assert.assertNotNull(this.mockMvc.perform(get(url, 1)).andExpect(status().isNotFound()));
    }

    @Test
    public void getByName_withValidName_shouldReturn200() throws Exception {
        Mockito.when(this.heroService.getByName(Mockito.any())).thenReturn(Arrays.asList(new Hero()));

        String url = "/api/v1/hero?name=:name";

        Assert.assertNotNull(this.mockMvc.perform(get(url, "loa")).andExpect(status().isOk()));
    }

}
