package es.mindata.challenge.repository;

import java.util.Locale;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.junit4.SpringRunner;

import es.mindata.challenge.model.entity.Hero;

@RunWith(SpringRunner.class)
@DataJpaTest
public class HeroRepositoryTest {
    private final String[]    HEROS = { "Logan", "Storm", "Superman" };
    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private HeroRepository    repository;

    @Before
    public void setUp() {
        for (int i = 0; i < HEROS.length; i++) {
            Hero hero = new Hero();

            hero.setName(String.valueOf(HEROS[i]));
            this.repository.save(hero);
        }
    }

    @Test
    public void findAll_shouldReturnHeroList() throws Exception {
        Assert.assertTrue(this.repository.findAll().size() == HEROS.length);
    }

    @Test
    public void findById_withInvalidId_shouldReturnOptionalEmpty() throws Exception {
        Assert.assertFalse(this.repository.findById(Long.MAX_VALUE).isPresent());
    }

    @Test(expected = InvalidDataAccessApiUsageException.class)
    public void findById_withNullId_shouldThrowInvalidDataAccessException() throws Exception {
        this.repository.findById(null).isPresent();
    }

    @Test
    public void findByName_withNameContaining_shouldReturnHero() throws Exception {
        Assert.assertFalse(this.repository.findByNameContainingIgnoreCase(String.valueOf(HEROS[0])).isEmpty());
    }

    @Test
    public void findByNameLower_withLowerName_shouldReturnHero() throws Exception {
        Assert.assertFalse(this.repository.findByNameContainingIgnoreCase(HEROS[0].toLowerCase(Locale.ROOT)).isEmpty());
    }

    @Test
    public void findByNameUpper_withUpperName_shouldReturnHero() throws Exception {
        Assert.assertFalse(this.repository.findByNameContainingIgnoreCase(HEROS[0].toUpperCase()).isEmpty());
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void save_withDuplicatedName_shouldThrowDataIntegrityViolationException() throws Exception {
        Hero hero = new Hero();

        hero.setName(HEROS[0]);
        this.repository.save(hero);
    }

}


