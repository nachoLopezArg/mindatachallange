package es.mindata.challenge.service;

import es.mindata.challenge.model.dto.HeroRequestDto;
import es.mindata.challenge.model.entity.Hero;
import es.mindata.challenge.model.exception.DuplicatedException;
import es.mindata.challenge.model.exception.InvalidException;
import es.mindata.challenge.model.exception.NotFoundException;
import es.mindata.challenge.repository.HeroRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
public class HeroServiceTest {

    @TestConfiguration
    static class HeroServiceTestConfiguration {
        @Bean
        public HeroService heroService() {
            return new HeroServiceImpl();
        }
    }

    @Autowired
    private HeroService heroService;

    @MockBean
    private HeroRepository heroRepository;

    @Test
    public void create_withValidHero_shouldPersistRegistry() throws Exception {
        HeroRequestDto heroRequestDto = new HeroRequestDto();
        heroRequestDto.setName(" mindata ");
        this.heroService.create(heroRequestDto);
    }

    @Test(expected = DuplicatedException.class)
    public void create_withDuplicatedHero_shouldThrowDuplicatedException() throws Exception {
        HeroRequestDto heroRequestDto = new HeroRequestDto();
        heroRequestDto.setName(" mindata ");
        Mockito.when(this.heroRepository.save(Mockito.any())).thenThrow(DataIntegrityViolationException.class);
        this.heroService.create(heroRequestDto);
    }

    @Test
    public void update_withValidHero_shouldUpdateRegistry() throws Exception {
        HeroRequestDto heroRequestDto = new HeroRequestDto();
        heroRequestDto.setName(" mindata ");
        Mockito.when(this.heroRepository.findById(1l)).thenReturn(Optional.of(new Hero()));
        this.heroService.update(1l, heroRequestDto);
    }

    @Test(expected = NotFoundException.class)
    public void update_withInvalidId_shouldThrowNotFoundException() throws Exception {

        // proxy object
        Mockito.when(this.heroRepository.findById(1l)).thenReturn(Optional.empty());
        this.heroService.update(5l, new HeroRequestDto());
    }

    @Test(expected = InvalidException.class)
    public void update_withNullId_shouldThrowInvalidException() throws Exception {
        Mockito.when(this.heroRepository.findById(null)).thenThrow(InvalidDataAccessApiUsageException.class);
        this.heroService.update(null, new HeroRequestDto());
    }

    @Test
    public void getById_withValidId_shouldGetRegistry() throws Exception {
        Mockito.when(this.heroRepository.findById(1l)).thenReturn(Optional.of(new Hero()));
        Assert.assertNotNull(this.heroService.getByID(1l));
    }

    @Test(expected = NotFoundException.class)
    public void getById_withInvalidId_shouldThrowNotFoundException() throws Exception {
        Mockito.when(this.heroRepository.findById(1l)).thenReturn(Optional.empty());
        this.heroService.getByID(1l);
    }

    @Test(expected = InvalidException.class)
    public void getById_withNullId_shouldThrowInvalidException() throws Exception {
        Mockito.when(this.heroRepository.findById(null)).thenThrow(InvalidDataAccessApiUsageException.class);
        this.heroService.getByID(null);
    }

}


